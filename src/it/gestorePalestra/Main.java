package it.gestorePalestra;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static it.gestorePalestra.Membro.Abbonamento.*;

public class Main {
    public static void main(String[] args) {

        GestionePalestra palestra = new GestionePalestra();

        Membro membro1 = palestra.registraNuovoMembro("alessandro", "masetti");
        Membro membro2 = palestra.registraNuovoMembro("mario", "rossi");
        Membro membro3 = palestra.registraNuovoMembro("silvia", "bianchi");

        palestra.assegnaAbbonamento(membro1, MENSILE, LocalDate.parse("2024-04-09"));
        palestra.assegnaAbbonamento(membro2, GIORNALIERO, LocalDate.parse("2024-04-07"));
        palestra.assegnaAbbonamento(membro3, ANNUALE, LocalDate.parse("2024-04-09"));

        Istruttore istruttore1 = new Istruttore("cristiano", "ronaldo", "01010", "bodybuilding", 15);
        Istruttore istruttore2 = new Istruttore("lionel", "messi", "0202202", "crossfit", 8);

        ClasseDiAllenamento classe1 = null;

        List<Membro> lista1 = new ArrayList<>();

        try {
            classe1 = palestra.aggiungiClasseAllenamento("crossfit", "10-12", 1, lista1);
        } catch (ClasseEsistenteExeption e) {
            System.out.println("la classe esiste già");
        }

        palestra.assegnaIstruttore(classe1, istruttore1);

        // mi aspetto che mi dica che l'abbonamento non è valido
        try {
            palestra.assegnaMembroClasse(membro2, classe1);
        } catch (ClassePienaException e) {
            System.out.println("la classe è piena");
        } catch (AbbonamentoNonValidoException e) {
            System.out.println("l'abbonameto non è valido");
        }






    }

    }

