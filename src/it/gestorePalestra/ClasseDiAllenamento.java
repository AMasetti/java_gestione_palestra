package it.gestorePalestra;

import java.util.ArrayList;
import java.util.List;

public class ClasseDiAllenamento {
    private String nomeClasse;
    private String orario;
    private List<Membro> listaIscritti;
    private Istruttore istruttore;
    private int capienza;


    public ClasseDiAllenamento(String nomeClasse, String orario, List<Membro> listaIscritti, Istruttore istruttore, int capienza) {
        this.nomeClasse = nomeClasse;
        this.orario = orario;
        this.listaIscritti = new ArrayList<>();
        this.istruttore = istruttore;
        this.capienza = capienza;
    }

    public ClasseDiAllenamento(String nomeClasse, String orario, int capienza, List<Membro> listaIscritti) {
        this.nomeClasse = nomeClasse;
        this.orario = orario;
        this.capienza = capienza;
        this.listaIscritti = new ArrayList<>();
    }

    /**
     * getter del nome della classe di allenamento
     * @return nome classe
     */
    public String getNomeClasse() {
        return nomeClasse;
    }

    /**
     * setter del nome della classe di allenamento
     * @param nomeClasse
     */
    public void setNomeClasse(String nomeClasse) {
        this.nomeClasse = nomeClasse;
    }

    /**
     * getter del'orario della classe di allenamento
     * @return orario classe
     */
    public String getOrario() {
        return orario;
    }

    /**
     * setter del'orario della classe di allenamento
     * @param orario
     */
    public void setOrario(String orario) {
        this.orario = orario;
    }

    /**
     * getter della lista di iscritti della classe di allenamento
     * @return lista iscritti
     */
    public List<Membro> getListaIscritti() {
        return listaIscritti;
    }

    /**
     * setter della lista di iscritti della classe di allenamento
     * @param listaIscritti
     */
    public void setListaIscritti(List<Membro> listaIscritti) {
        this.listaIscritti = listaIscritti;
    }

    /**
     * getter dell'istruttore della classe di allenamento
     * @return istruttore
     */
    public Istruttore getIstruttore() {
        return istruttore;
    }

    /**
     * setter dell'istruttore della classe di allenamento
     * @param istruttore
     */
    public void setIstruttore(Istruttore istruttore) {
        this.istruttore = istruttore;
    }

    /**
     * getter della capienza della classe di allenamento
     * @return capienza
     */
    public int getCapienza() {
        return capienza;
    }

    /**
     * setter della capienza della classe di allenamento
     * @param capienza
     */
    public void setCapienza(int capienza) {
        this.capienza = capienza;
    }
}
