package it.gestorePalestra;

/**
 * Eccezione che si verifica in caso si provi a creare una classe allenamento già esistente
 */
public class ClasseEsistenteExeption extends Exception{
    public ClasseEsistenteExeption(){
        super();
    }



}
