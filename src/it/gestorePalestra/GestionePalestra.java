package it.gestorePalestra;

import java.util.*;
import java.time.LocalDate;

public class GestionePalestra {
    private HashMap<String, Membro.Abbonamento> abbonamenti;
    private ArrayList<ClasseDiAllenamento> classi;

    //utilizzo il costruttore vuoto per questa classe
    public GestionePalestra() {
        this.abbonamenti = new HashMap<>();
        this.classi = new ArrayList<>();
    }

    public HashMap<String, Membro.Abbonamento> getAbbonamenti() {
        return abbonamenti;
    }

    public void setAbbonamenti(HashMap<String, Membro.Abbonamento> abbonamenti) {
        this.abbonamenti = abbonamenti;
    }

    public ArrayList<ClasseDiAllenamento> getClassi() {
        return classi;
    }

    public void setClassi(ArrayList<ClasseDiAllenamento> classi) {
        this.classi = classi;
    }

    /**
     * Metodo che permette la creazione di un membro e il suo ID, creato randomicamente con controllo sull'esistenza
     * @param nome
     * @param cognome
     * @return Membro
     */
    //creo un metodo che attraverso l'inserimento di nome e cognome genera un ID e crea un'istanza di Membro
    public Membro registraNuovoMembro(String nome, String cognome){
        Membro membro = null;
        Random random = new Random();
        String iDmembro = Integer.toString(random.nextInt());
        while (abbonamenti.containsKey(iDmembro)) {
            iDmembro = Integer.toString(random.nextInt());
            }
        System.out.println("l'ID del nuovo membro è " + iDmembro);
        return new Membro(nome, cognome, iDmembro);
        }

    /**
     * Metodo che permette di asegnare l'abbonamento a un membro preso in input, con inserimento data manuale
     * @param membro
     * @param abbonamento
     * @param data
     */
    public void assegnaAbbonamento(Membro membro, Membro.Abbonamento abbonamento, LocalDate data){
        membro.setDataIscrizione(data);
        membro.setAbbonamento(abbonamento);
        abbonamenti.put(membro.iDmembro, membro.abbonamento);
        System.out.println("al membro " + membro + "è stato assegnato l'abbonamento" + abbonamento);
    }

    /**
     * Metodo per aggiungere una classe di allenamento con controllo che impedisce di inserire due classi con lo stesso nome e orario.
     * @param nome
     * @param orario
     * @param capienza
     * @param listaIscritti
     * @return ClasseDiAllenamento
     * @throws ClasseEsistenteExeption
     */
    public ClasseDiAllenamento aggiungiClasseAllenamento(String nome, String orario, int capienza, List<Membro> listaIscritti) throws ClasseEsistenteExeption {
        boolean flag = false;
        Iterator<ClasseDiAllenamento> iterator = classi.iterator();
        while (iterator.hasNext()) { // controllo che non esista una classe uguale
            ClasseDiAllenamento classe = iterator.next();
            if (classe.getNomeClasse() == nome && classe.getOrario() == orario) {
                flag = true;
            }
        }
        if (flag == false) {
            System.out.println("la classe " + nome + " è stata creata");
            return new ClasseDiAllenamento(nome, orario, capienza, listaIscritti);
        }
        else throw new ClasseEsistenteExeption();
    }

    /**
     * Metodo per assegnare l'istruttore, ad istruttore assegnato aggiungo anche la classe nella lista.
     * @param classe
     * @param istruttore
     */
    public void assegnaIstruttore(ClasseDiAllenamento classe, Istruttore istruttore){
        classe.setIstruttore(istruttore);
        classi.add(classe); //aggiungo il corso alla lista di classi
        System.out.println("l'istruttore " + istruttore + " è stato aggiunto alla classe " + classe);
    }

    /**
     * Metodo per assegnare un membro a una classe, con controllo di capienza della classe e validità dell'abbonamento
     * @param membro
     * @param classe
     * @throws ClassePienaException
     * @throws AbbonamentoNonValidoException
     */
    public void assegnaMembroClasse(Membro membro, ClasseDiAllenamento classe)
            throws ClassePienaException, AbbonamentoNonValidoException {
    //controllo che l'abbonamento non sia scaduto nelle tre casistiche
        switch(membro.abbonamento){
           case GIORNALIERO:
               if (membro.getDataIscrizione().plusDays(1).isBefore(LocalDate.now())){
                   throw new AbbonamentoNonValidoException();}
           case MENSILE:
                if (membro.getDataIscrizione().plusMonths(1).isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException();}
            case ANNUALE:
                if (membro.getDataIscrizione().plusYears(1).isBefore(LocalDate.now())){
                    throw new AbbonamentoNonValidoException();}
            default:
                if (classe.getListaIscritti().size() >= classe.getCapienza()) { //controllo se la classe è piena
                    throw new ClassePienaException();
                }
                else {
                    classe.getListaIscritti().add(membro);
                    System.out.println("il membro " + membro + " è stato aggiunto alla classe " + classe);    }
        }

    }








}

