package it.gestorePalestra;

/**
 * Eccezione che si verifica in caso si provi a iscrivere un membro a una classe allenamento già piena
 */
public class ClassePienaException extends Exception {
    public ClassePienaException(){
        super();
    }
}

