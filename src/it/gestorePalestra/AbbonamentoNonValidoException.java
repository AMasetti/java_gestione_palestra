package it.gestorePalestra;


public class AbbonamentoNonValidoException extends Exception {
    /**
     * Eccezione che si verifica in caso si provi a iscrivere a una classe un utente con abbonamento scaduto
     */
    public AbbonamentoNonValidoException(){
        super();
    }
}
