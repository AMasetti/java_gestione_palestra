package it.gestorePalestra;

import java.time.LocalDate;

public class Membro {
    protected String nome;
    protected String cognome;
    protected String iDmembro;

    protected Abbonamento abbonamento;



    enum Abbonamento {
        GIORNALIERO,
        MENSILE,
        ANNUALE
    }

    protected LocalDate dataIscrizione;

    public Membro(String nome, String cognome, String iDmembro, LocalDate dataIscrizione, Abbonamento abbonamento) {
        this.nome = nome;
        this.cognome = cognome;
        this.iDmembro = iDmembro;
        this.dataIscrizione = dataIscrizione;
        this.abbonamento = abbonamento;
    }

    // creo un costruttore senza data e abbonamento, mi sarà utile per l'estensione (Istruttore)
    public Membro(String nome, String cognome, String iDmembro) {
        this.nome = nome;
        this.cognome = cognome;
        this.iDmembro = iDmembro;
    }

    public Membro(String nome, String cognome, String iDmembro, LocalDate dataIscrizione) {
        this.nome = nome;
        this.cognome = cognome;
        this.iDmembro = iDmembro;
        this.dataIscrizione = dataIscrizione;
    }
    public Membro() {

    }


    /**
     * getter del nome del membro
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * setter del nome del membro
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * getter del cognome del membro
     * @return cognome
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * setter del cognome del membro
     * @param cognome
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }


    /**
     * getter dell'ID del membro
     * @return ID membro
     */
    public String getiDmembro() {
        return iDmembro;
    }

    /**
     * setter dell'ID del membro
     * @param iDmembro
     */
    public void setiDmembro(String iDmembro) {
        this.iDmembro = iDmembro;
    }

    /**getter della data di iscrizione del membro
     * @return data iscrizione
     */
    public LocalDate getDataIscrizione() {
        return dataIscrizione;
    }

    /**
     * setter della data di iscrizione del membro
     * @param dataIscrizione
     */
    public void setDataIscrizione(LocalDate dataIscrizione) {
        this.dataIscrizione = dataIscrizione;
    }

    /**
     * getter del tipo di abbonamento del membro
     * @return tipo abbonamento
     */
    public Abbonamento getAbbonamento() {
        return abbonamento;
    }

    /**
     * setter del tipo di abbonamento del membro
     * @param abbonamento
     */
    public void setAbbonamento(Abbonamento abbonamento) {
        this.abbonamento = abbonamento;
    }
}
