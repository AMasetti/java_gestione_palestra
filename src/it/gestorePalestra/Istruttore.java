package it.gestorePalestra;

public class Istruttore extends Membro{
    private String specializzazione;
    int anniEsp;
    public Istruttore(String nome, String cognome, String iDmembro, String specializzazione, int anniEsp) {
        super(nome, cognome, iDmembro);
        this.specializzazione = specializzazione;
        this.anniEsp = anniEsp;
    }

    /**
     * getter della specializzazione dell'istruttore
     * @return specializzazione
     */
    public String getSpecializzazione() {
        return specializzazione;
    }

    /**setter della specializzazione dell'istruttore
     * @param specializzazione
     */
    public void setSpecializzazione(String specializzazione) {
        this.specializzazione = specializzazione;
    }

    /**
     * getter degli anni di esperienza dell'istruttore
     * @return anni di esperienza
     */
    public int getAnniEsp() {
        return anniEsp;
    }

    /**
     * setter degli anni di esperienza dell'istruttore
     * @param anniEsp
     */
    public void setAnniEsp(int anniEsp) {
        this.anniEsp = anniEsp;
    }
}
